(function ($) {
	$(document).ready(function () {
        var $root = $('html, body');
        $('a[href^="#"]').click(function() {
            var href = $.attr(this, 'href');
            $root.animate({
                scrollTop: $(href).offset().top - 0
            }, 500, function () {
                window.location.hash = href;
            });
            return false;
        });
        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();
        
            if (scroll >= 10) {
                $('body').addClass('header-active');
            } else {
                $('body').removeClass('header-active');
            }
        });
       
        $('.owl-awards').owlCarousel({
            loop:true,
            margin:10,
            autoplay:false,
            responsiveClass:true,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1,
                },
                576:{
                    items:3,
                },
                768:{
                    items:4,
                },
                1000:{
                    items:5,
                    
                }
            }
        })
	});
})(jQuery);

$("#custerror").hide();
$("#custerror1").hide();

$.validator.addMethod("validateEmail", function(value, element) {
    return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
});

$.validator.addMethod("website", function(value, element) {
    return this.optional(element) || /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$/.test(value);
});

$('[data-type="number-only"]').keyup(function () {
    var value = $(this).val();
    value = value.replace(/\D/g, "");
    $(this).val(value);
});

$('[data-type="text-only"]').keyup(function () {
    var value = $(this).val();
    value = value.replace(/[0-9`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, "");
    $(this).val(value);
});

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

const submitBtnObj = document.querySelector('#submitBtn');
if(submitBtnObj) {
    submitBtnObj.addEventListener("click", (e) => {
        e.preventDefault();
        $("#contactForm").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true,
                    validateEmail:true
                },
                contactno: {
                    required: true,
                    minlength: 10,
                },
                companywebsite: {
                    required: true,
                    website: true,
                },
                cityname: {
                    required: true,
                },
                checkbox: {
                    required:true
                },
                
            },
            messages: {
                name: "Name is required",
                email: {
                    required: "Email is required",
                    email: "Please enter valid email address"
                },
                contactno: {
                    required: "Contact number is required",
                    minlength: "Enter valid contact number"
                },
                companywebsite: {
                    required: "Company website is required",
                    website: "Invalid website"
                },
                cityname: {
                    required: "Please select a city name",
                },
                checkbox:{
                    required: "Please agree terms and conditions"
                }
            }
        });

        if ($("#contactForm").valid()) {
             if(($("input[name='checkbox']:checked").length==0)) {
                $("#custerror1").slideDown();
                    setTimeout(function () {
                        $("#custerror1").slideUp();
                    }, 3000);
                return false;
             }

             var utmMedium = getParameterByName('utm_medium') === null ? 'digital' : getParameterByName('utm_medium');
             var utmSource = getParameterByName('utm_source') === null ? 'google' : getParameterByName('utm_source');
             var utmCampaign = getParameterByName('utm_campaign') === null ? 'elearningcontent01' : getParameterByName('utm_campaign');
             const campData = {
                "name": $("#name").val(),
                "email": $("#email").val(),
                "contact_no": $("#contactno").val(),
                "company_website": $("#companywebsite").val(),
                "city_name": $("#cityname").val(),
                "utm_medium": utmMedium,
                "utm_source": utmSource,
                "utm_campaign": utmCampaign,
                "consent": ($("input[name='checkbox']:checked").length==0) ? 0 : 1
            }
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: campData,
                success: function (response) {
                    response = JSON.parse(response);
                    if (201 == response.statusCode) {
                        window.location.replace("thankyou.php");
                        $(".loader").css({
                            display: "none"
                        });
                    } else {
                         $("#custerror").slideDown();
                        setTimeout(function () {
                         $("#custerror").slideUp();
                        }, 3000);
                    }
                }
            });
        };
    });
}
